#!/usr/bin/perl -w
use Module::Load;
use YAML::XS (LoadFile);
use Data::Dumper;
use Getopt::Long;
use Pod::Usage;

my $cfg_file = "config.yaml";
my $cmd="usage";
my $out="result";
my $suffix = "number";
my $verbosity = 0;
my ($db, $target, $ext);

sub cmd_usage() {
    pod2usage(1);
}

# processing config and options
GetOptions(
    "help|man|usage" => sub { cmd_usage(); },
    "verbose+" => \$verbosity,
    "config=s" => \$cfg_file,
    "cmd=s" => \$cmd,
    "out=s" => \$out,
    "db=s@" => \$db,
    "dbgroup=s@" => \$db,
    "target=s@" => \$target,
    "suffix=s" => \$suffix,
    "ext=s" => \$ext
) or usage();

my $cfg = LoadFile($cfg_file);
$cfg->{verbosity} = $verbosity;
print_v(1, "verbosity level is $verbosity\n");
my $dbh;
eval {
    my $modname = "dbhelpers_$cfg->{main}->{dbtype}";
    autoload $modname;
    $dbh = $modname->new($cfg);
    print_v(1, $dbh->version() . " loaded\n");
    1;
} or do {
    error_exit("unknown database type '$cfg->{main}->{dbtype}', or failed to load dbhelper library");
};
if (!defined($ext)) {
    $ext = "";
} else {
    $ext = ".$ext";
}
if (exists(&{"cmd_$cmd"})) {
    "cmd_$cmd"->();
} elsif ($cmd =~ /^list_(.*)/) {
    list_group($1);
} else {
    error_exit("unknown command: $cmd");
}

# main functions
sub list_group {
    my $group = shift;
    if (!exists($cfg->{databases}->{$group})) {
        error_exit("unknown database group: " . $group);
    }
    for my $type (@{$cfg->{databases}->{$group}}) {
        print "$type->{name}\n";
    }
}

sub cmd_backup {
    if (!defined($db)) {
        error_exit("no database given, use --db option");
    }
    for (my $i = 0; $i < scalar @$db; ++$i) {
        my $suff = scalar @$db > 1 ? compute_suffix($i) : "";
        if ($dbh->dump_db($db->[$i], "$out$suff$ext")) {
            print "some error occurred while backup\n";
        }
    }
}

# helpers
sub compute_suffix {
    my $i = (shift) + 1;
    if ($suffix eq "number") {
        return "-$i";
    } else {
        error_exit("invalid suffix type");
    }
}

sub error_exit {
    die shift . "\n";
}

sub print_v {
    my ($v, $what) = @_;
    if ($verbosity >= $v) {
        print $what;
    }
}

__END__

=head1 NAME

dbtools - database tool suite
    
=head1 SYNOPSIS

dbtools [options]

=head1 OPTIONS

=over 8

=item B<--cmd>

Defines the command to execute

=item B<--db>

=item B<--out>

=item B<--suffix>

=item B<-help>

=item B<-man>

=back

=head1 DESCRIPTION

A database tool suite.

=head1 EXAMPLES

B<dbtools.pl --cmd=backup --db=test --db=test2 --out=backup --suffix=date>

Backups two databases into files suffixed with the current date and time.

=cut

