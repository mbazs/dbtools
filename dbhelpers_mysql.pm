package dbhelpers_mysql;
use base Exporter;
our @EXPORT = qw/table_exists dump_db/;

sub version {
    "dbhelper for MySQL ver 1.0.0";
}

sub new {
    my ($class, $cfg) = @_;
    return bless { cfg => $cfg }, $class;
}

sub dump_db {
    my ($self, $dbname, $out) = @_;
    my $cmd = "mysqldump -h$self->{cfg}->{main}->{host} -u$self->{cfg}->{main}->{user} -p$self->{cfg}->{main}->{password} --databases $dbname  > \"$out\" 2>/dev/null";
#    print "$cmd\n";
#    print_v(1, $cmd);
    `$cmd`;
}

sub table_exists {
    my $self = shift;
    0;
}

#sub print_v {
#    my ($v, $what) = @_;
#    if ($verbosity}>= $v) {
#        print $what;
#    }
#}

1;

